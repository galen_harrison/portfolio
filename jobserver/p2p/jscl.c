#include <stdlib.h>
#include <stdio.h>
#include "csapp.h"

typedef void *tmain(void *);

int count(int n) { //test function
  
   int i;

   for (i = 0; i < n; i++) {
     Sleep(1000); //proc must take time so that actual job dist is tested
   }

   return n;
}

typedef struct _lwrap_t {
  int port;
  int *j;
  pthread_mutex_t *m;
  char *go;
} lwrap_t;

void lhandle(int connfd, pthread_mutex_t *m, int avail, int *j) {

  size_t n;
  char buf[MAXLINE];
  char in[MAXLINE];
  int job;
  rio_t rio;

  Rio_readinitb(&rio, connfd);
  printf("lhandle\n");
  while ((n = Rio_readlineb(&rio,buf,MAXLINE)) != 0) {
    sscanf(buf,"%s",in);
    if (strcmp(in, "JOB") == 0) {//if listen hears a job
      sscanf(buf,"%s %i",in,&job);
      Rio_writen(connfd, "OK", 2);
      if (avail) {
        printf("accepting job\n");
        count(job);
        avail = !avail;
      } else {
        printf("passing job\n");
        pthread_mutex_lock(m);
        (*j) = job;
        pthread_mutex_unlock(m);
        printf("passed: %i\n", (*j));
      };
    } else if (strcmp(in, "LOG") == 0) {
      printf("log soon to come\n");
    }
  };
}

void listen_conn(lwrap_t *l) {

  int listenfd, connfd, clientlen;
  struct sockaddr_in clientaddr;
  struct hostent *hp;
  char *haddrp;
  int avail = 0;
  

  printf("Opening account...\n");
  listenfd = Open_listenfd(l->port);
  
    clientlen = sizeof(clientaddr);
  	(*l->go) = 'c';
    connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
	  printf("Locked\n");
    hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, 
			   sizeof(clientaddr.sin_addr.s_addr), AF_INET);
	  haddrp = inet_ntoa(clientaddr.sin_addr);
	  printf("Connection with %s (%s).\n", hp->h_name, haddrp);
	  lhandle(connfd,l->m,avail,l->j);
	  Close(connfd);
    printf("Exiting listen\n");
}

typedef struct ch_wrap_t {
  char *host;
  char *go;
  int port;
  int *j;
  pthread_mutex_t *m;
} ch_wrap_t;

void spinoff(ch_wrap_t *c) {
  if ((*c->go) == 'c') { 
    int clientfd = Open_clientfd(c->host, c->port);
    char ack[MAXLINE];
    rio_t rio;

    Rio_readinitb(&rio, clientfd);
   
      //printf("c->j %i\n",(*c->j));
    if ((*c->j) > 0) {
        printf("ch_handle passing: %i\n",(*c->j));
        char tmp[MAXLINE];
        char job[4] = "JOB ";
        char *msg = (char *)malloc(MAXLINE*sizeof(char));
        sprintf(tmp, "%d",(*c->j));
        msg = strcat(job,tmp);
        printf("Passing job\n");
        Rio_writen(clientfd,msg,strlen(msg));
        Rio_readlineb(&rio, ack, MAXLINE);
        if (strcmp(ack, "OK") != 0) {
          fprintf(stderr,"Server error\n");
        };
        pthread_mutex_lock(c->m);
        (*c->j) = 0;
        pthread_mutex_unlock(c->m);
     };
     printf("Exiting spinoff\n");
  };
}

void ch_handle(ch_wrap_t *c) {
  if ((*c->go) == 'c') {
    int clientfd;
    char buf[MAXLINE];
    char in[MAXLINE];
    char ack[MAXLINE];
    rio_t rio;

    clientfd = Open_clientfd(c->host,c->port);
    Rio_readinitb(&rio, clientfd);

    if (Fgets(buf,MAXLINE, stdin) != NULL) {
      printf("scanning1\n");
      sscanf(buf, "%s", in);
      if (strcmp(in, "JOB") == 0) {
        
        Rio_writen(clientfd,buf,strlen(buf));
        Rio_readlineb(&rio, ack, MAXLINE);

        if (strcmp(ack, "OK") != 0) {
          fprintf(stderr,"Server error\n");
        };
      } else {
         printf("Please input a job or log request\n");
      }
    };
    Close(clientfd);
  };
}

int main(int argc, char **argv) {
  
  if (argc != 4) {
    fprintf(stderr,"usage: %s <listen> <talkHost> <talkPort>\n",argv[0]);
    exit(0);
  };
  
  ch_wrap_t *c = (ch_wrap_t *)malloc(sizeof(ch_wrap_t));
  lwrap_t *l = (lwrap_t *)malloc(sizeof(lwrap_t));

  pthread_mutex_t m;
  pthread_mutex_init(&m,NULL);
  int j = 0;
  c->host = argv[2];
  c->port = atoi(argv[3]);
  c->j = &j;
  c->m = &m;
  l->port = atoi(argv[1]);
  l->j = &j;
  l->m = &m;
  char *go = (char *)malloc(sizeof(char));
  l->go = go;
  c->go = go;

  pthread_t lid;
  pthread_t cid;
  pthread_t sid;
  char buf[MAXLINE];
  
  while(1) {
    printf("scanning for input\n");
    Pthread_create(&lid,NULL,(tmain *)listen_conn,(void *)l);
    Pthread_create(&cid,NULL,(tmain *)ch_handle,(void *)c);
    Pthread_create(&sid,NULL,(tmain *)spinoff,(void *)c);
    Pthread_join(sid,NULL);
    Pthread_join(cid,NULL);
    Pthread_join(lid,NULL);
  };
  printf("exiting main\n");
  exit(0);
}
