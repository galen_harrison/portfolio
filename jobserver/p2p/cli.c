#include "csapp.h"

int count(int n) { //test function
  
   int i;

   for (i = 0; i < n; i++) {
     Sleep(1000); //proc must take time so that actual job dist is tested
   }

   return n;
}


  /*
void in_handle(int port) {

  int listenfd, connfd, clientlen;
  struct sockaddr_in clientaddr; 
  struct hostent *hp;
  char *haddrp;
  
  printf("Opening account...\n");
  listenfd = Open_listenfd(port);
  
  while (1) {
	  clientlen = sizeof(clientaddr);
	  connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

  	hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, 
			   sizeof(clientaddr.sin_addr.s_addr), AF_INET);
	  haddrp = inet_ntoa(clientaddr.sin_addr);
	  printf("Connection with %s (%s).\n", hp->h_name, haddrp);
  

	  Close(connfd);
  }
    exit(0);
}
void out_handle(char *host, int port) {

    int clientfd; 
    char buf[MAXLINE];
    char ack[MAXLINE];
    char command[MAXLINE];
    rio_t rio;
    char *out;

    clientfd = Open_clientfd(host, port);
    Rio_readinitb(&rio, clientfd);

    while (Fgets(buf, MAXLINE, stdin) != NULL) {
      sscanf(buf,"%s",command);
    }
    Close(clientfd); 
    exit(0);
}
*/
typedef struct _buf_t {
  rio_t ir;
  rio_t cr;
  int connfd;
  int clientfd;
} buf_t;

buf_t *connect_lc(int lport, char *chost, int cport) {
  
  //Listens and returns the listen buffer (ibuf) and connecting buffer
  
  buf_t *r = (buf_t *)malloc(sizeof(buf_t));
  char go;
  int client_fd,listenfd, connfd, clientlen;
  struct sockaddr_in clientaddr;
  struct hostent *hp;
  char *haddrp;
  rio_t irio,crio;

  listenfd = Open_listenfd(lport);
  printf("listening\n");
  clientlen = sizeof(clientaddr);
  
  scanf(" %c",&go);
  
  if (go == 'c') {
    client_fd = Open_clientfd(chost, cport);
    Rio_readinitb(&crio, client_fd);
  } else {
    fprintf(stderr,"wrong input, to connect use c");
  };
    
  connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
  printf("connfd alloc'd\n");
  hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
  haddrp = inet_ntoa(clientaddr.sin_addr);
  
  Rio_readinitb(&irio, connfd); 
  r->ir = irio;
  r->cr = crio;
  r->connfd = connfd;
  r->clientfd = client_fd;
  
  return r;
}
void listen_handle(int listenfd) {
  
  size_t n;
  struct hostent *hp;
  struct sockaddr_in clientaddr;
  int clientlen = sizeof(clientaddr);
  int connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
  hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,sizeof(clientaddr.sin_addr.s_addr), AF_INET);
  char  *haddrp = inet_ntoa(clientaddr.sin_addr);
  
  rio_t *rio;
  char buf[MAXLINE];

  Rio_readinitb(&rio, connfd);
  if ((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {

void job_push(buf_t *iob, char *cmd) {

  char ack[MAXLINE];
  printf("beginning write\n");
  Rio_writen(iob->clientfd, cmd, strlen(cmd));
  printf("pushing job: %s",cmd); 
  Rio_readlineb(&iob->cr, ack, MAXLINE);

  if (strcmp("Y",ack) == 0){
    printf("Job accepted\n");
  } else if (strcmp("N",ack) == 0) {
    printf("Server busy, passing along\n");
  } else {
    fprintf(stderr,"server error");
  };
} 
  
void input_handle(buf_t *iob) {

  //scan the three buffers for input. 
  //local buf accepts 2 commands, JOB and LOG
  //inbuf and outbuf accept the same commands
  //PUSH: checks if a server is available, and if it is, pushes the job
  //      if it isn't, the server passes the job check to the next server in the chain
  //LOG: queries the log for the two neighbors. If there are no results that contradict,
  //     the local log is updated, and the order to update is sent to the other servers.

  char lbuf[MAXLINE]; //local input buffer
  char cmd[MAXLINE]; //command buffer
  char ibuf[MAXLINE];
  int available = 0; //use a balancer type implementation
 

  //local input handling

  if (Fgets(lbuf,MAXLINE,stdin) != NULL){
    sscanf(lbuf,"%s",cmd);
    if (strcmp(cmd,"JOB") == 0) {
      printf("Job pushed: %s",cmd);
      job_push(iob,cmd);
    } else if (strcmp(cmd,"LOG") == 0) {
     // log_dump();
      printf("not yet installed\n");
    } else {
      printf("Not recognized\n");
    }
  }

  size_t n;
  printf("phase 2\n");
  if ((n = Rio_readlineb(&iob->cr,ibuf,MAXLINE)) != 0) {
    sscanf(ibuf, "%s",cmd);
    if (strcmp("JOB",cmd) == 0) {
      if (!available) {
        printf("received\n");
        Rio_writen(iob->connfd,"N",1);
        job_push(iob,cmd); //may need to copy cmd contents to avoid
      } else {
        printf("received and accepted\n");
        Rio_writen(iob->connfd,"Y",1);
        int k;
        available = !available;
        sscanf(cmd,"%n",&k);
        count(n);
        available = !available;
      }
    } else {
      printf("Unrecognized entry\n");
    }; 
 };
}
int main(int argc, char **argv) {
  
  //When each job scl is opened, it waits until 
  //it listens on its "in" port and only connects
  //once receives a manual invocation to its out port

  //To invoke job server client (jscl), 
  // ./jscl <listen port> <conn host> <conn port> 
  printf("starting\n");
  buf_t *iob = connect_lc(atoi(argv[1]), argv[2], atoi(argv[3]));
  printf("connected\n");
  while (1) {
    input_handle(iob);        
  };
}
