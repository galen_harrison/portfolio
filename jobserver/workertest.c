#include "csapp.h"

int main(int argc, char **argv) {

  //Quick and dirty for the moment, will need more sophisticated protocal later

  int clientfd,port;
  char *host;
  char buf[MAXLINE];
  char ack[MAXLINE];

  rio_t rio;
  
  host = argv[1];

  clientfd = Open_clientfd(host, atoi(argv[2]));
  Rio_readinitb(&rio, clientfd);
  sprintf(buf,"BEGIN\n"); 
  Rio_writen(clientfd, buf,strlen(buf));
  printf("waiting for feedback\n");
  Rio_readlineb(&rio,ack,MAXLINE);
  printf("Complete");
  printf("%s",ack);
  sprintf(buf, "JOB 5\n");
  Rio_writen(clientfd, buf, strlen(buf));
  Rio_readlineb(&rio,ack,MAXLINE);
  printf("%s",ack);
  Rio_writen(clientfd, "JOB 6\n", 5);
}
