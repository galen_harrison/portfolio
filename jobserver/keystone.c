#include "csapp.h"

/*this code is the input node for the circular network */
typedef void *tmain(void *);
typedef struct _tinfo_t {
  int iport,oport,j,flag;
  char *ohost;
  pthread_cond_t c;
  pthread_mutex_t m;
} tinfo_t;

void listen_in(tinfo_t *t, rio_t *rio, int connfd) {

  size_t n;
  int j;
  char buf[MAXLINE];
  char in[MAXLINE];
  
  while (Rio_readlineb(rio, buf, MAXLINE) != 0) {
    sscanf(buf, "%s", in);
    if (strcmp(in,"BEGIN") == 0) {
      pthread_cond_broadcast(&t->c);
      sprintf(buf,"OK\n");
      Rio_writen(connfd, buf,strlen(buf));
    } else if (strcmp(in, "JOB") == 0) {
      sscanf(buf,"%s %i",in,&j);
      printf("job received: %i\n", j);
      Rio_writen(connfd,"OK\n",3);
    //  pthread_mutex_lock(&t->m);
      t->j = j; //availability not checked
    //  pthread_mutex_unlock(&t->m);
      printf("Done\n");
    } else {
      printf("error\n");
    }
  };
}

void handle_in(tinfo_t *t) {
  
  int listenfd, connfd, clientlen;
  struct sockaddr_in clientaddr;
  struct hostent *hp;
  char *haddrp;
  rio_t rio;

  listenfd = Open_listenfd(t->iport);
  printf("Listening %d\n", listenfd);
  clientlen = sizeof(clientaddr);
  connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
  hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
      sizeof(clientaddr.sin_addr.s_addr), AF_INET);
  printf("Connection with %s.\n",hp->h_name);
  Rio_readinitb(&rio, connfd);
  while (1) {
    listen_in(t, &rio, connfd);
  };
}
       
void handle_out(tinfo_t *t) {
  printf("waiting for contact\n");
  pthread_cond_wait(&t->c,&t->m);
  printf("contact initiated\n");
  int clientfd;
  rio_t rio;
  char ack[MAXLINE];
  char buf[MAXLINE];
  char tmp[MAXLINE];

  //connect to next worker
  clientfd = Open_clientfd(t->ohost,t->oport);
  Rio_readinitb(&rio, clientfd);
  
  Rio_writen(clientfd,"BEGIN\n",6);
  Rio_readlineb(&rio,ack,MAXLINE);
  printf("%s\n",ack);
  printf("chain linked\n");

  while (1) { //unlike workers, keystone just passes jobs along
    if (t->j > 0) {
      printf("Passing along: %i\n", t->j);
//      pthread_mutex_lock(&t->m);
      sprintf(buf,"JOB ",4);
      sprintf(tmp, "%d\n",t->j);
      strcat(buf,tmp);
      printf("%s",buf);
      Rio_writen(clientfd,buf,strlen(buf));
      Rio_readlineb(&rio,ack,MAXLINE);
      printf("%s",ack);
      if (strcmp(ack,"OK\n") == 0) {
        t->j = 0;
      } else {
        printf("Bad returns\n");
      }
  //    pthread_mutex_unlock(&t->m);
    };
  };
      
}

void handle_loc(tinfo_t *t) {
  
  char buf[MAXLINE];
  char command[MAXLINE];
  int j;
  while (Fgets(buf,MAXLINE,stdin) != NULL) {
    sscanf(buf, "%s",command); 
    if (strcmp(command, "JOB") == 0) {
      printf("Job submitted: ");
      //pthread_mutex_lock(&t->m);
      sscanf(buf,"%s %i",command,&j);
      printf("%i\n",j);
      t->j = j;
      //pthread_mutex_unlock(&t->m);
    } else if (strcmp(command, "BEGIN") == 0){
      pthread_cond_broadcast(&t->c);
      printf("Enter JOB <n>\n");
    } else {
      printf("Input not recognized, use JOB <n>\n");
    };
  };
  printf("Ending input\n");
}

int main(int argc, char **argv) {

  pthread_t oid,iid,lid;
  tinfo_t *t = (tinfo_t *)malloc(sizeof(tinfo_t));
  
  pthread_mutex_init(&t->m,NULL);
  pthread_cond_init(&t->c,NULL);
  t->iport = atoi(argv[1]);
  t->ohost = argv[2];
  t->oport = atoi(argv[3]);
  
  Pthread_create(&oid,NULL,(tmain *)handle_out,(void *)t);
  Pthread_create(&iid,NULL,(tmain *)handle_in, (void *)t);
  Pthread_create(&lid,NULL,(tmain *)handle_loc,(void *)t);
  printf("Threads generated\n");
  Pthread_join(oid,NULL);
  Pthread_join(iid,NULL);
  Pthread_join(lid,NULL);
  exit(0);
  
} 
