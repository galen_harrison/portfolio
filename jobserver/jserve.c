#include "csapp.h"

/*This code is used for the worker nodes.
  It represents an evolution of the jscl code, as
  handling 3 forms of input for every server was 
  creating relativley complex bugs.

  There are two connections that a worker needs
  to deal with, an in and an out.

*/
typedef void *tmain(void *);
int count(int n) { //test function
   Sleep(n); //proc must take time so that actual job dist is tested
   return n;
}

typedef struct _tinfo_t {
  int iport,oport,j,flag;
  char *ohost;
  pthread_cond_t c;
  pthread_mutex_t m;
} tinfo_t;

void listen_in(tinfo_t *t, rio_t *rio, int connfd) {

  size_t n;
  int j;
  char buf[MAXLINE];
  char in[MAXLINE];
  
  while (Rio_readlineb(rio, buf, MAXLINE) != 0) {
    sscanf(buf, "%s", in);
    if (strcmp(in,"BEGIN") == 0) {
      pthread_cond_broadcast(&t->c);
      sprintf(buf,"OK\n");
      Rio_writen(connfd, buf,strlen(buf));
    } else if (strcmp(in, "JOB") == 0) {
      sscanf(buf,"%s %i",in,&j);
      printf("job received: %i\n", j);
      Rio_writen(connfd,"OK\n",3);
      //pthread_mutex_lock(&t->m);
      if (t->flag) {
        printf("Job accepted\n");
        t->flag = !t->flag; //for some reason the mutex is causing things to freeze up
        count(j); //this protocol isn't set up to handle return work just yet
      } else {
        t->j = j;
        t->flag = !t->flag;
      };
      //pthread_mutex_unlock(&t->m);
      printf("Done\n");
    } else {
      printf("error\n");
    }
  };
}

void handle_in(tinfo_t *t) {
  
  int listenfd, connfd, clientlen;
  struct sockaddr_in clientaddr;
  struct hostent *hp;
  char *haddrp;
  rio_t rio;

  listenfd = Open_listenfd(t->iport);
  printf("Listening %d\n", listenfd);
  clientlen = sizeof(clientaddr);
  connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
  hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
      sizeof(clientaddr.sin_addr.s_addr), AF_INET);
  printf("Connection with %s.\n",hp->h_name);
  Rio_readinitb(&rio, connfd);
  while (1) {
    listen_in(t, &rio, connfd);
  };
}
       
void handle_out(tinfo_t *t) {
  printf("waiting for contact\n");
  pthread_cond_wait(&t->c,&t->m);
  printf("contact initiated\n");
  int clientfd;
  rio_t rio;
  char ack[MAXLINE];
  char buf[MAXLINE];
  char tmp[MAXLINE];

  //connect to next worker
  clientfd = Open_clientfd(t->ohost,t->oport);
  Rio_readinitb(&rio, clientfd);
  
  Rio_writen(clientfd,"BEGIN\n",6);
  Rio_readlineb(&rio,ack,MAXLINE);
  printf("%s",ack);
  printf("chain linked\n");

  while (1) {
    if (t->j > 0) {
      printf("Passing along: %i\n", t->j);
      //pthread_mutex_lock(&t->m);
      sprintf(buf,"JOB ",4);
      printf("f1\n");
      sprintf(tmp, "%d\n",t->j);
      strcat(buf,tmp);
      printf("%s\n",buf);
      Rio_writen(clientfd,buf,strlen(buf));
      Rio_readlineb(&rio,ack,MAXLINE);
      printf("%s",ack);
      if (strcmp(ack,"OK\n") == 0) {
        t->j = 0;
      } else {
        printf("Bad returns\n");
      }
      //balancer work distribution scheme
     // pthread_mutex_unlock(&t->m);
    };
  };
      
}

int main(int argc, char **argv) {

  int avail = 0;
  pthread_t oid,iid;
  tinfo_t *t = (tinfo_t *)malloc(sizeof(tinfo_t));
   
  pthread_mutex_init(&t->m,NULL);
  pthread_cond_init(&t->c,NULL);
  printf("start\n");
  t->iport = atoi(argv[1]);
  t->ohost = argv[2];
  t->oport = atoi(argv[3]);
  Pthread_create(&oid, NULL,(tmain *)handle_out,(void *)t);
  Pthread_create(&iid, NULL,(tmain *)handle_in,(void *)t); 
  printf("threads created\n");
  Pthread_join(oid,NULL);
  printf("out completed, waiting on in\n");
  Pthread_join(iid,NULL);
  exit(0);
}
