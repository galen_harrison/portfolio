# waves.py implements the random ocean wave height field
# described in the Tessendorf paper found: 
# 
# http://graphics.ucsd.edu/courses/rendering/2005/jdewall/tessendorf.pdf
#
# There are also shader techniques which can give
# this rendering more realism, those are next to be implemented

from numpy.random import normal
from cmath import *
from geometry import point,vector
g = 9.8 #gravitational constant
w = vector(1.0,0.0,0.0) #north wind direction
Vw = 30.0 #wind speed 

# k is a vector (kx, kz, 0) 

def Ph(k): #phillips wave spectrum, used for generating height field

	global Vw,w,g
	
	L = Vw*Vw/g

	# The paper doesn't specify a good value for A or give any hints
	# as to good values to use

	return 5000*(exp(-1/(k.norm()*L)**2)*abs(k.unit().dot(w))**2)/k.norm()**4

def h0(k):

	return sqrt(Ph(k)/2)*complex(normal(0,1),normal(0,1))

def h(k,t):

	# use omega(k) = sqrt(gk) so assume deep water and 
	# large magnitude waves 
	if k.norm() != 0:
		a1 = h0(k)*exp(complex(0,1)*sqrt(g*k.norm())*t)
		a2 = h0(k.neg()).conjugate()*exp(complex(0,-1)*sqrt(g*k.norm())*t)

		return a1+a2
	else:
		return 0 #not sure how to handle this
def h_spatial(x,t,klist): #from the frequency resolved h, get the fourier transformation

	l = [h(k,t)*exp((k[0]*x.x+k[1]*x.z)*complex(0,1)) for k in klist]
	return sum(l)

def mesh_gen(Lx,Lz,M,N,t):
 
	xlist = []
	klist = []

	# populate xlist and klist

	for i in range(-M//2,M//2):
		r = []
		for j in range(-int(N/2),int(N/2)):
			x = j*Lx/N
			z = i*Lz/M
			
			r.append(point(x,0,z))

			kx = 2*pi*j/Lx
			kz = 2*pi*i/Lz

			klist.append(vector(kx,kz,0))
		xlist.append(r)
	return [[h_spatial(x,t,klist) for x in r] for r in xlist],xlist
 
def obj_gen(Lx,Lz,M,N):

	mesh,xlist = mesh_gen(Lx,Lz,M,N,0) # since this will be a static object, ignore time
	
	# transform to points in a structured manner

	pts = []

	for r in mesh:
		
		l = []

		for p in xlist[mesh.index(r)]:
			
			x = mesh[mesh.index(r)][xlist[mesh.index(r)].index(p)]
			a = sqrt(x*x.conjugate())/((M-1)*(N-1))
			arg = phase(x)

			l.append((p.x,(a*cos(arg)).real,p.z))
	
		pts.append(l)

	outfile = open('waves.obj','w')

	ind = 1
	line = {}

	for l in pts:
		for p in l:
			outfile.write("v "+str(p[0])+" "+str(p[1])+" "+str(p[2])+"\n")
			line[p] = ind
			ind+=1
	for l in pts[:-1]:
		for p in l[:-1]:
			outfile.write("f "+str(line[p])+" "+str(line[l[l.index(p)+1]])+" "+str(line[pts[pts.index(l)+1][l.index(p)+1]])+"\n")
			outfile.write("f "+str(line[p])+" "+str(line[pts[pts.index(l)+1][l.index(p)+1]])+" "+str(line[pts[pts.index(l)+1][l.index(p)]])+"\n")				
	outfile.close()
