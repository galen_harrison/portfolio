#version 120

varying vec3 n;  
varying vec3 P;  
varying vec3 material_c;
varying vec3 bcoord;

uniform vec3 light;      // position of a point light source
uniform vec3 eye;        // position of the eyepoint
uniform int wires;

void main() {

  
  vec3 diff = fwidth(bcoord);

  bool edge = (bcoord[0] < diff[0]*0.5 
	       || bcoord[1] < diff[1]*0.5 
	       || bcoord[2] < diff[2]*0.5);

  if (wires==1 && edge) {

    gl_FragColor = vec4(0.8,0.8,0.3,1.0);

  } else {

    float reflectivity;

    vec3 upwelling = vec3(0,0.2,0.3);
    vec3 sky = vec3(0.69,0.84,1);
    vec3 air = vec3(0.1,0.1,0.1);
    float nsnell = 1.34;
    float Kdiffuse = 0.91;
    
    vec3 l = normalize(light - P);
    vec3 e = normalize(eye - P);
   
    float costhetai = abs(dot(l,n));
    float thetai = acos(costhetai);
    float sinthetat = sin(thetai)/nsnell;
    float thetat = asin(sinthetat);

    if (thetai == 0.0) {
        reflectivity = (nsnell-1)/(nsnell+1);
        reflectivity = reflectivity*reflectivity;
    } else {
        float fs = sin(thetat - thetai)/sin(thetat+thetai);
	float ts = tan(thetat - thetai)/tan(thetat+thetai);
        reflectivity = 0.5*(fs*fs+ts*ts);
    }

    float dist = length(eye-P)*Kdiffuse;
    dist = exp(-dist);
 
    vec3 color = dist*(reflectivity*sky+(1-reflectivity)*upwelling)+(1-dist)*air; 
    gl_FragColor = vec4(color,1.0);
  }
}
