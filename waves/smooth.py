from math import cos,pi
import we
import geometry as geo

def alpha(n):

	assert type(n) == int, "Non integer input"
	return (n*beta(n))/(1-beta(n))

def beta(n): 

	assert type(n) == int, "Non integer input"
	return (5./8.)-((3+(2*cos(2*pi/n)))/8)**2

def vertex_handle(v):

	assert type(v) == we.vertex, "Invalid refinement object"
	
	if is_cone(v):
#		print("cone")
		return cone_refine(v)
	else:
#		print("fan")
		return fan_handle(v)
	
def cone_refine(v):

	neighbors = [e.vertex(1).position for e in v.around()]
	n = len(neighbors)
	coefficients = n*[1/(alpha(n)+n)]
	p = v.position.combos(coefficients,neighbors)

	return p 

def is_cone(v): #check to see if v.edge is part of the face of the last element in the fan
	
	neighbors = [e for e in v.around()]
	last = neighbors[-1]
	if v.edge.twin in last.face.edges():
		return True
	else:
		return False

def fan_handle(v):

	v0 = v.edge.vertex(1).position
	vl = [e.vertex(1).position for e in v.around()][-1]

	return v.position.combos([.125,0.125],[v0,vl])

def edge_handle(e):

	if e.twin == None:
#		print("exterior edge")
		return e.vertex(0).position.combo(0.5,e.vertex(1).position)

	else:
#		print("internal edge")

		v0 = e.vertex(0).position
		v1 = e.vertex(1).position
		f0 = e.next.vertex(1).position 
		f1 = e.twin.next.vertex(1).position
		
		return v0.combos([0.125,0.375,0.125],[f0,v1,f1])

 
