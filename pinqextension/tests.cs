using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using PINQ;
using shtat;

namespace tests
{
	class Program
	{
		//tests for spread and median

		//spread_test(epsilon,number of queries,size of database,generating parameter)
		public  static List<string> spread_test(double epsilon, int r, int n, double sigma)
		{

			//generate data
			Random rand = new Random();
			List<double> data = new List<double>();
			Stopwatch t = new Stopwatch();

			for (int i = 0; i < n; i++)
			{
				double u1 = rand.NextDouble();
				double u2 = rand.NextDouble();
				double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
				data.Add(sigma*sample);
			}

			data.Sort();

			List<string> ret = new List<string>();
			PINQueryable<double> sec = new PINQueryable<double>(data.AsQueryable(),null);

			double iqr = Quarts.IQR(data);

			for (int s = 0; s < r; s++)
			{
				t.Start();
				double? res = sec.spread(epsilon,x=>x);
				t.Stop();
				
				string ts = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",t.Elapsed.Hours, t.Elapsed.Minutes, t.Elapsed.Seconds,t.Elapsed.Milliseconds / 10);
				
				if (res.HasValue)
					ret.Add("spread "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+iqr.ToString()+" "+res.Value.ToString()+" "+ts);
				else
					ret.Add("spread "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+iqr.ToString()+" null "+ts);
				t.Reset();
			}
			Console.WriteLine("Done");
			return ret;
		}
		
		//med_test(epsilon,number of queries,size of database,generating parameter)
		public static List<string> med_test(double epsilon, int r, int n, double mu, double sigma)
		{

			//generate data
			Random rand = new Random();
			List<double> data = new List<double>();

			for (int i = 0; i < n; i++)
			{
				double u1 = rand.NextDouble();
				double u2 = rand.NextDouble();
				double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
				data.Add(mu+sigma*sample);
			}

			data.Sort();
			double m = Quarts.Median(data);
			List<string> ret = new List<string>();
			PINQueryable<double> sec = new PINQueryable<double>(data.AsQueryable(),null);
			for (int s = 0; s < r; s++)
			{
				double? res = sec.med(epsilon,x=>x,sec.spread(epsilon, x=>x));
				if (res.HasValue)
					ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" "+res.Value.ToString());
				else
					ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" null");
			}
			Console.WriteLine("Done");
			return ret;
		}

		public static List<string> smed_test(double epsilon, int r, int n, double mu, double sigma)
		{

			//generate data
			Random rand = new Random();
			List<double> data = new List<double>();

			for (int i = 0; i < n; i++)
			{
				double u1 = rand.NextDouble();
				double u2 = rand.NextDouble();
				double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
				data.Add(mu+sigma*sample);
			}

			data.Sort();
			double m = Quarts.Median(data);
			List<string> ret = new List<string>();
			PINQueryable<double> sec = new PINQueryable<double>(data.AsQueryable(),null);
			for (int s = 0; s < r; s++)
			{
		///		double res = sec.smed(epsilon,x=>x,0);
		///		ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" 0 "+res.ToString());
				
		///		double res = sec.smed(epsilon,x=>x,1);
		///		ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" 1 "+res.ToString());
				
				double res = sec.smed(epsilon,x=>x,2);
				ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" 2 "+res.ToString());
			}
			Console.WriteLine("Done");
			return ret;
		}
		public static List<double[]> multi_draw(double mu, double sigma, int d, int n)
		{
			List<double[]> ret = new List<double[]>();
			Random rand = new Random();
	
			for (int i = 0; i < n; i++)
			{
				double[] entry = new double[d];
				for (int j = 0; j < d-1; j++)
				{
					double u1 = rand.NextDouble();
					double u2 = rand.NextDouble();
					double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
					entry[j] = mu+sigma*sample;
				}
				entry[d-1] = entry.Sum();
				ret.Add(entry);
			};
			return ret;
		}
		static public Tuple<List<string>, List<string>> LR_test(double mu, double sigma, int d, int n, double epsilon, int r)
		{
			var data = multi_draw(mu,sigma,d,n);
			
			PINQueryable<double[]> sec = new PINQueryable<double[]>(data.AsQueryable(),null);
			List<string> r1 = new List<string>();
			List<string> r2 = new List<string>();	
			for (int i = 0; i < r; i++)
			{
				double?[] sclr = sec.SCLR(epsilon, x=>x);
				string res1 = "sclr "+epsilon.ToString()+" "+n.ToString()+" "+i.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+string.Join(" ",sclr.Select(x=>{ if (x.HasValue)
									return x.Value.ToString();
								else						
									return "null";
								})); 
				double[] plr = sec.PLR(epsilon, x=>x);
				string res2 = "plr "+epsilon.ToString()+" "+n.ToString()+" "+i.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+string.Join(" ",plr);
				r1.Add(res1);
				r2.Add(res2);
			}

			return Tuple.Create(r1,r2);
		}
			
			
		static void Main(string[] args)
		{
			
			List<string> scres = new List<string>();
			IEnumerable<int> nlist = new List<int>(new int[3] {100,500,1000});
			
			foreach (int n in nlist)
			{
				Console.WriteLine("Calculating for "+n.ToString());
				scres.AddRange(smed_test(Convert.ToDouble(args[0]),100,n,0,1));
			}
			System.IO.File.WriteAllLines(args[1],scres);
		}
	}
}	
