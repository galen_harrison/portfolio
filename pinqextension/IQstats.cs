using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Convert;

namespace shtat
{
	public static class Quarts
	{
		/// finds 25th quartile

		public static int lowQ(this List<double> list)
		{
			double index = 0.25 * (list.Count()+1);
			index = Math.Floor(index) > 0? Math.Floor(index)- 1: 0;
			return (int)index;
		}

		/// finds the 75th quartile
		
		public static int hiQ(this List<double> list)
		{
			double index = 0.75 * (list.Count()+1);
			index = Math.Floor(index)-1;
			return (int)index;
		}

		/// interquartile range

		public static double IQR(this List<double> list)
		{
			return list.ElementAt(hiQ(list))-list.ElementAt(lowQ(list));
		}

		/// part of spread. To move IQR(D) out of its bin, it's necessary to both 
		/// minimize and maximize. minI counts how many points need to be changed so 
		/// that IQR(D) is in a in lower bin than its original

		public static int minI(this List<double> list, double ub, double lb)
		{
			/// Imin is the bin lower bound for IQR(D)
			/// then find maximum number of data points within the 25th and 75th
			/// percentiles such that they have range Imin

			int l = list.lowQ();
			int u = list.hiQ();
			int ct = 0;
			int n = list.Count();
			double Imin = Math.Pow(1.0+(1.0/Math.Log(n)),lb);

			int candidates = list.Count(x => ((list.ElementAt(l) < x) && (x < (list.ElementAt(u) - Imin))));

			for (int i = l; i < l+candidates; i++) 
			{
				int poss = list.Count(x => ((list.ElementAt(i) < x) && (x < (list.ElementAt(i) + Imin))));
				if (poss > ct) {
					ct = poss;
				} 	
			
			}

			return (u-l-ct);
		}
		
		/// similarly maxI does the same but by trying to move IQR(D) above its bin 

		public static int maxI(this List<double> list, double lb, double ub)
		{
		
			/// Define Imax, the bin upper bound, scan across data to find max number
			/// of points which will satisfy Imax.

			int l = list.lowQ();
			int u = list.hiQ();
			int ct = 0;
			int n = list.Count();
			double w = 1.0+(1.0/Math.Log(n));
			double Imax = Math.Pow(w,ub);

			int sp = list.Count(x => (x < (list.ElementAt(u)-Imax)));
			for (int i = sp; i<=l; i++)
			{
				int poss = list.Count(x => (list.ElementAt(i) < x) && (x < (list.ElementAt(i)+Imax)));
				if (poss > ct)
				{
					ct = poss;
				}
			}
			return ct-(u-l);
		}

		/// answer to query Q_0 from Dwork and Lei 2009 paper. lb and ub allow for different
		/// partitions.

		public static int mm(this List<double> list, double lb, double ub)
		{
			int ctmin = minI(list,lb,ub);
			int ctmax = maxI(list,lb,ub);
			
			if (ctmax <= 0)
				return ctmin;
			else
				return Math.Min(ctmin,ctmax);
		}

		/// answer to Q_1 from median algorithm for 1st partition
		/// no need to scan, just find number of elements in the bin over mInd,
		/// the number of elements in the bin under mInd, and return the minimum

		/// probably could be written more idiomatically to C#

		public static int Q1_1(this List<double> list, double s)
		{
			double med = Median(list);
			int mInd = list.Count()/2;
			double h = s*Math.Pow(list.Count(),-1.0/3.0);
			double k = Math.Floor(med/h);
			double entry = list.ElementAt(mInd);
			int under = 0; //number of elements in box under median
			int over = 0; //number of elements in box over median
			int ind = mInd; //search index (technically we could just use this to get under and over, but it's neater like this

			//get count of number of elements under
			while (entry >= k*h)
			{
				if (ind == 0) {
					break;
				}
				under++;
				ind--;
				entry = list.ElementAt(ind);
			}
			//get over count
			ind = mInd;
			entry = list.ElementAt(ind+1);
			
			while(entry < (k+1)*h)
			{
				if (ind == list.Count()-1) {
					break;
				};
				over++;
				ind++;
				entry = list.ElementAt(ind);
			}
			return Math.Min(under,over);
		} 

		/// same as Q1_1 but for a different binning. Code is the same but for k-0.5,k+0.5 instead. 

		public static int Q1_2(this List<double> list, double s)
		{
			double med = Median(list);
			int mInd = list.Count()/2;
			double h = s*Math.Pow(list.Count(),-1.0/3.0);
			double k = Math.Floor((med/h)+0.5);
			double entry = list.ElementAt(mInd);
			int under = 0; //number of elements in box under median
			int over = 0; //number of elements in box over median
			int ind = mInd; //search index (technically we could just use this to get under and over, but it's neater like this

			//get count of number of elements under
			
			while (entry >= (k-0.5)*h)
			{
				if (ind == 0)
					break;
				under++;
				ind--;
				entry = list.ElementAt(ind);
			}
	
			//get over count
			ind = mInd;
			entry = list.ElementAt(ind+1);
			
			while(entry < (k+0.5)*h)
			{
				if (ind == list.Count()-1)
					break;
				over++;
				ind++;
				entry = list.ElementAt(ind);
			}

			return Math.Min(under,over);
		} 

		public static double Median(this IEnumerable<double> source)
		{
			if (source.Count() == 0)
			{
			    throw new InvalidOperationException("Cannot compute median for an empty set.");
			}

			var sortedList = from number in source
					 orderby number
					 select number;

			int itemIndex = (int)sortedList.Count() / 2;

			if (sortedList.Count() % 2 == 0)
			{
			    // Even number of items. 
			    return (sortedList.ElementAt(itemIndex) + sortedList.ElementAt(itemIndex - 1)) / 2;
			}
			else
			{
			    // Odd number of items. 
			    return sortedList.ElementAt(itemIndex);
			}
		}	
		
		/// used for the short cut linear regression. Returns X and Y partitions.
			
		public static Tuple<List<double[][]>,List<double[]>> RPART(List<double[]> data) ///randomly partitions into floor(n/p) lists
		{
			int p = data[0].GetLength(0)-1;	
			int n = data.Count();
			int m = n/p;
			Random rand = new Random();
	
			List<double[][]> xlist = new List<double[][]>(); //xlist is the list of all the X partitions
			List<double[]> ylist = new List<double[]>(); //ylist contains all the Y vectors
			List<double[]> tmp = data.Select(x=>x).ToList();

			//randomly draw from tmp until m partitions filled

			for (int i = 0; i < m; i++)
			{
				double[][] X = new double[p][];
				double[] Y = new double[p];

				for (int j = 0; j < p; j++)
				{
					int d = rand.Next(0,tmp.Count());
					X[j] = new double[p];
					
					Y[j] = tmp[d][p];
					Array.Copy(tmp[d],0,X[j],0,p);
					tmp.Remove(tmp[d]);				 
				}
				
				xlist.Add(X);
				ylist.Add(Y);
			}
			return Tuple.Create(xlist,ylist);
		}
		/*InvertMatrix, LUPSolve and LUPDecomposition are all from http://www.rkinteractive.com/blogs/SoftwareDevelopment/post/2013/05/21/Algorithms-In-C-Finding-The-Inverse-Of-A-Matrix.aspx */
		public static double[][] InvertMatrix(double[][] A)
		{
		    int n = A.Length;
		    //e will represent each column in the identity matrix
		    double[] e;
		    //x will hold the inverse matrix to be returned
		    double[][] x = new double[n][];
		    for (int i = 0; i < n; i++)
		    {
			x[i] = new double[A[i].Length];
		    }
		    /*
		    * solve will contain the vector solution for the LUP decomposition as we solve
		    * for each vector of x.  We will combine the solutions into the double[][] array x.
		    * */
		    double[] solve;

		    //Get the LU matrix and P matrix (as an array)
		    Tuple<double[][], int[]> results = LUPDecomposition(A);

		    double[][] LU = results.Item1;
		    int[] P = results.Item2;

		    /*
		    * Solve AX = e for each column ei of the identity matrix using LUP decomposition
		    * */
		    for (int i = 0; i < n; i++)
		    {
			e = new double[A[i].Length];
			e[i] = 1;
			solve = LUPSolve(LU, P, e);
			for (int j = 0; j < solve.Length; j++)
			{
			    x[j][i] = solve[j];
			}
		    }
		    return x;
		}
		public static double[] LUPSolve(double[][] LU, int[] pi, double[] b)
		{
		    int n = LU.Length-1;
		    double[] x = new double[n+1];
		    double[] y = new double[n+1];
		    double suml = 0;
		    double sumu = 0;
		    double lij = 0;

		    /*
		    * Solve for y using formward substitution
		    * */
		    for (int i = 0; i <= n; i++)
		    {
			suml = 0;
			for (int j = 0; j <= i - 1; j++)
			{
			    /*
			    * Since we've taken L and U as a singular matrix as an input
			    * the value for L at index i and j will be 1 when i equals j, not LU[i][j], since
			    * the diagonal values are all 1 for L.
			    * */
			    if (i == j)
			    {
				lij = 1;
			    }
			    else
			    {
				lij = LU[i][j];
			    }
			    suml = suml + (lij * y[j]);
			}
			y[i] = b[pi[i]] - suml;
		    }
		    //Solve for x by using back substitution
		    for (int i = n; i >= 0; i--)
		    {
			sumu = 0;
			for (int j = i + 1; j <= n; j++)
			{
			    sumu = sumu + (LU[i][j] * x[j]);
			}
			x[i] = (y[i] - sumu) / LU[i][i];
		    }
		    return x;
		}

		/*
		* Perform LUP decomposition on a matrix A.
		* Return L and U as a single matrix(double[][]) and P as an array of ints.
		* We implement the code to compute LU "in place" in the matrix A.
		* In order to make some of the calculations more straight forward and to 
		* match Cormen's et al. pseudocode the matrix A should have its first row and first columns
		* to be all 0.
		* */
		public static Tuple<double[][], int[]> LUPDecomposition(double[][] A)
		{
		    int n = A.Length-1;
		    /*
		    * pi represents the permutation matrix.  We implement it as an array
		    * whose value indicates which column the 1 would appear.  We use it to avoid 
		    * dividing by zero or small numbers.
		    * */
		    int[] pi = new int[n+1];
		    double p = 0;
		    int kp = 0;
		    int pik = 0;
		    int pikp = 0;
		    double aki = 0;
		    double akpi = 0;
			    
		    //Initialize the permutation matrix, will be the identity matrix
		    for (int j = 0; j <= n; j++)
		    {
			pi[j] = j;
		    }

		    for (int k = 0; k <= n; k++)
		    {
			/*
			* In finding the permutation matrix p that avoids dividing by zero
			* we take a slightly different approach.  For numerical stability
			* We find the element with the largest 
			* absolute value of those in the current first column (column k).  If all elements in
			* the current first column are zero then the matrix is singluar and throw an
			* error.
			* */
			p = 0;
			for (int i = k; i <= n; i++)
			{
			    if (Math.Abs(A[i][k]) > p)
			    {
				p = Math.Abs(A[i][k]);
				kp = i;
			    }
			}
			if (p == 0)
			{
			    throw new Exception("singular matrix");
			}
			/*
			* These lines update the pivot array (which represents the pivot matrix)
			* by exchanging pi[k] and pi[kp].
			* */
			pik = pi[k];
			pikp = pi[kp];
			pi[k] = pikp;
			pi[kp] = pik;
				
			/*
			* Exchange rows k and kpi as determined by the pivot
			* */
			for (int i = 0; i <= n; i++)
			{
			    aki = A[k][i];
			    akpi = A[kp][i];
			    A[k][i] = akpi;
			    A[kp][i] = aki;
			}

			/*
			    * Compute the Schur complement
			    * */
			for (int i = k+1; i <= n; i++)
			{
			    A[i][k] = A[i][k] / A[k][k];
			    for (int j = k+1; j <= n; j++)
			    {
				A[i][j] = A[i][j] - (A[i][k] * A[k][j]); 
			    }
			}
		    }
		    return Tuple.Create(A,pi);
		}
		/*
		* A, y represent a set of data points where y is the vector of data for the dependent variable and
		* A is the set of vectors of data for the independent variables.  The result will be the coefficient
		* vector.
		* */
		public static double[] LeastSquares(double[][] A, double[] y)
		{
		    //solve will be the coefficient vector
		    double[] solve = new double[A[0].Length];
		    //find transpose of A
		    double[][] Atranspose = MatrixTranspose(A);
		    //multiply A transpose by A
		    double[][] AtA = MatrixMultiply(Atranspose, A);
		    //multiply A transpose by y
		    double[] Aty = MatrixMultiply(Atranspose, y);
		    //find LUP decomposition of AtA
		    Tuple<double[][], int[]> results = LUPDecomposition(AtA);
		    double[][] LU = results.Item1;
		    int[] P = results.Item2;

		    //Solve for LU, P, and Aty
		    solve = LUPSolve(LU, P, Aty);

		    return solve;
		}		
		/*
		 Multiply two matrices together and return the resulting matrix
		* */
		public static double[][] MatrixMultiply(double[][] A, double[][] B)
		{
		    double[][] C = new double[A.Length][];
		    for (int i = 0; i < A.GetLength(0); i++)
		    {
			C[i] = new double[B[0].Length];
		    }

		    if (A[0].Length != B.Length)
		    {
			throw new Exception("Incompatable Dimensions");
		    }
		    else
		    {

			for (int i = 0; i <= A.Length - 1; i++)
			{
			    for (int j = 0; j <= A.Length - 1; j++)
			    {
				C[i][j] = 0;
				for (int k = 0; k <= A[0].Length - 1; k++)
				{
				    C[i][j] = C[i][j] + A[i][k] * B[k][j];
				}
			    }
			}
		    }

		    return C;
		}

		/*
		* Multiply a matrix by a vector and return the resulting vector
		* */
		public static double[] MatrixMultiply(double[][] A, double[] B)
		{
		    double[] C = new double[A[0].Length];
			    
		    if (A[0].Length != B.Length)
		    {
			throw new Exception("Incompatable Dimensions");
		    }
		    else
		    {

			for (int i = 0; i <= A.Length - 1; i++)
			{
			    for (int j = 0; j <= A.Length - 1; j++)
			    {
				C[i] = 0;
				for (int k = 0; k <= A[0].Length - 1; k++)
				{
				    C[i] = C[i] + A[i][k] * B[k];
				}
			    }
			}
		    }

		    return C;
		}
		//Find the transpose of a matrix
		public static double[][] MatrixTranspose(double[][] A)
		{
		    //Get the number of rows of A
		    int n = A.Length;
		    //Get the number of columns of A
		    int m = A[0].Length;

		    //The new matrix will be mXn
		    double[][] Atranspose = new double[m][];

		    for (int i = 0; i < m; i++)
		    {
			Atranspose[i] = new double[n];
		    }

		    for (int i = 0; i < n; i++)
		    {
			for (int j = 0; j < m; j++)
			{
			    Atranspose[j][i] = A[i][j];
			}
		    }
		    return Atranspose;
		}
		
		/// testing function

		static void bench_counters(string[] args)
		{
			Console.WriteLine("Setting up test");
			Random random = new Random();
			List<double> list = new List<double>();

			int n = Convert.ToInt32(args[0]);

			for (int i = 0; i < n; i++) {
				list.Add(random.NextDouble()*5);
			}
 		
			Console.WriteLine("List initialized");	
			list.Sort();
			int uq = hiQ(list);
			int lq = lowQ(list);
			double w = 1.0+(1.0/Math.Log(list.Count()));
			double lw = Math.Log(IQR(list),w);
			double k = Math.Floor(lw);
			int maxct = maxI(list, k, k+1);
			int minct = minI(list,k,k+1);
			int r = mm(list,k,k+1);
			Console.WriteLine("LQ value: {0}  UQ value: {1} IQR value: {2}", list[lq], list[uq], IQR(list));
			Console.WriteLine("Min: {0}  Max: {1} Result: {2}",minct,maxct,r);
		}
	
		/// method for neatly calculating A(k)(x) for smed

		static public double ak(List<double> values, int k, int t, int m)
		{
			if ((m+t) < values.Count() && (m+t-k-1) >= 0)
				return values.ElementAt(m+t)-values.ElementAt(m+t-k-1);
			else if (m+t >= values.Count() && m+t-k-1 >= 0)
				return values.Max()-values.ElementAt(m+t-k-1);
			else if (m+t < values.Count() && m+t-k-1 < 0)
				return values.ElementAt(m+t)-values.Min();
			else if (m+t >= values.Count() && m+t-k-1 < 0)
				return values.Max() - values.Min();
			else
				throw new Exception("ak error");
		}

		/// uk(x) for first S^* approximation

		static public double uk(List<double> values, int k, int m)
		{
			if ((m+k+1) < values.Count() && (m-k-1) >= 0)
				return values.ElementAt(m+k+1)-values.ElementAt(m-k-1);
			else if (m+k+1 >= values.Count() && m-k-1 >= 0)
				return values.Max()-values.ElementAt(m-k-1);
			else if (m+k+1 < values.Count() && m-k-1 < 0)
				return values.ElementAt(m+k+1)-values.Min();
			else if (m+k+1 >= values.Count() && m-k-1 < 0)
				return values.Max() - values.Min();
			else
				throw new Exception("ak error");
		}
		/// out dated testing procedures for some of these functions.
	
		static void Main(string[] args)
		{
		
			
			Console.WriteLine("Setting up test");
			Random random = new Random();
			List<double[]> list = new List<double[]>();
			
			int n = Convert.ToInt32(args[0]);
			int p = Convert.ToInt32(args[1]);

			for (int i = 0; i < n; i++) {
				
				double[] entry = new double[p];
				
				for (int j = 0; j < p; j++) {
					entry[j] = random.NextDouble()*10;
					Console.Write("{0} ",entry[j]);
				}
				Console.WriteLine();
				list.Add(entry);
			}

			var ret = RPART(list);
			int m = n/(p-1);
			double[][] invX;
			double[] row;

			for (int j = 0; j < m; j++)
			{
				Console.WriteLine("Part {0}",j);
				for(int k = 0; k < p-1; k++)
				{
					Console.Write("X:\t");
					Console.Write(string.Join(",",ret.Item1[j][k]));
					Console.WriteLine("\t Y:\t{0}",ret.Item2[j][k]);
				}

				invX = InvertMatrix(ret.Item1[j]);
				
				for (int x = 0; x < invX.Length; x++)
				{
					row = invX[x];
					Console.Write(string.Join("\t",row));
					Console.WriteLine();
				}

			} 
			
		}		
	}
}
