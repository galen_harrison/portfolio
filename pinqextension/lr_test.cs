using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using PINQ;
using shtat;

namespace tests
{
	class Program
	{
		//tests for spread and median

		//spread_test(epsilon,number of queries,size of database,generating parameter)
		public  static List<string> spread_test(double epsilon, int r, int n, double sigma)
		{

			//generate data
			Random rand = new Random();
			List<double> data = new List<double>();

			for (int i = 0; i < n; i++)
			{
				double u1 = rand.NextDouble();
				double u2 = rand.NextDouble();
				double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
				data.Add(sigma*sample);
			}

			data.Sort();

			List<string> ret = new List<string>();
			PINQueryable<double> sec = new PINQueryable<double>(data.AsQueryable(),null);

			double iqr = Quarts.IQR(data);

			for (int s = 0; s < r; s++)
			{
				double? res = sec.spread(epsilon,x=>x);
				if (res.HasValue)
					ret.Add("spread "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+iqr.ToString()+" "+res.Value.ToString());
				else
					ret.Add("spread "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+iqr.ToString()+" null");
			}
			Console.WriteLine("Done");
			return ret;
		}
		
		//med_test(epsilon,number of queries,size of database,generating parameter)
		public static List<string> med_test(double epsilon, int r, int n, double mu, double sigma)
		{

			//generate data
			Random rand = new Random();
			List<double> data = new List<double>();

			for (int i = 0; i < n; i++)
			{
				double u1 = rand.NextDouble();
				double u2 = rand.NextDouble();
				double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
				data.Add(mu+sigma*sample);
			}

			data.Sort();
			double m = Quarts.Median(data);
			List<string> ret = new List<string>();
			PINQueryable<double> sec = new PINQueryable<double>(data.AsQueryable(),null);
			for (int s = 0; s < r; s++)
			{
				double? res = sec.med(epsilon,x=>x,sec.spread(epsilon, x=>x));
				if (res.HasValue)
					ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" "+res.Value.ToString());
				else
					ret.Add("med "+epsilon.ToString()+" "+n.ToString()+" "+s.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+m.ToString()+" null");
			}
			Console.WriteLine("Done");
			return ret;
		}

		public static List<double[]> multi_draw(double mu, double sigma, int d, int n)
		{
			List<double[]> ret = new List<double[]>();
			Random rand = new Random();
	
			for (int i = 0; i < n; i++)
			{
				double[] entry = new double[d];
				for (int j = 0; j < d-1; j++)
				{
					double u1 = rand.NextDouble();
					double u2 = rand.NextDouble();
					double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
					entry[j] = mu+sigma*sample;
				}

				double uf = rand.NextDouble();
				double ug = rand.NextDouble();
				double fn = Math.Sqrt(-2.0*Math.Log(uf))*Math.Sin(2.0*Math.PI*ug);
				entry[d-1] = entry.Sum()+fn*0.1;
				ret.Add(entry);
			};
			return ret;
		}
		static public double Rsq(List<double[]> data, double[] raw_model)
		{

			List<double> predicted = new List<double>();
			double ssres = 0;
			double sstot = 0;
			double yavg = data.Sum(x=>x[x.Length-1])/data.Count();
			double[][] model = new double[1][];

			model[0] = raw_model.Concat(new double[1]{0}).ToArray();

			foreach (double[] entry in data)
			{

				//get the predicted value
				double fi = Quarts.MatrixMultiply(model,entry)[0];
				sstot = sstot+Math.Pow(entry[entry.Length-1]-yavg,2);
				ssres = ssres+Math.Pow((entry[entry.Length-1] - fi),2);

			}
			return 1 - (ssres/sstot);  
		}
		static public double[] lr (List<double[]> data)
		{
			double[][] X = new double[data.Count()][];
			double[] Y = new double[data.Count()];

			for (int i = 0; i < data.Count(); i++)
			{
				X[i] = new double[data[i].Length - 1];
				Y[i] = data[i][data[i].Length-1];
				Array.Copy(data[i],0,X[i],0,data[i].Length-1);

			}

			return Quarts.LeastSquares(X,Y);
		}	
		static public double R2 (List<double[]> data, double[] raw_model, double[] raw_model2)
		{

			double ss_n = 0;
			double ss_p = 0;
			double[][] model = new double[1][];
			double[][] model2 = new double[1][];

			model[0] = raw_model.Concat(new double[1]{0}).ToArray();
			model2[0] = raw_model2.Concat(new double[1]{0}).ToArray();			
			
			foreach (double[] entry in data)
			{

				//get the predicted value
				double fi = Quarts.MatrixMultiply(model,entry)[0];
				double gi = Quarts.MatrixMultiply(model2,entry)[0];

				ss_n = ss_n + Math.Pow(gi-entry[entry.Length-1],2);
				ss_p = ss_p + Math.Pow(fi-entry[entry.Length-1],2);
			}
			Console.WriteLine("model2 - "+string.Join(",",model2[0]));
			Console.WriteLine("ss_n - {0} ss_p -{1}",ss_n,ss_p);
			return ss_n/ss_p;  
		}	
		static public List<string> LR_test(double mu, double sigma, int d, int n, double epsilon, int r)
		{
			var data = clean_data(d,n);
			var m = lr(data);

			Stopwatch s1 = new Stopwatch();
			Stopwatch s2 = new Stopwatch();
	
			PINQueryable<double[]> sec = new PINQueryable<double[]>(data.AsQueryable(),null);
			List<string> r1 = new List<string>();
			List<string> r2 = new List<string>();	

			double rs = Rsq(data,m);

			for (int i = 0; i < r; i++)
			{
				s1.Start();
				double?[] sclr = sec.SCLR(epsilon, x=>x);
				s1.Stop();
				double[] model = sclr.Select(x =>{if(x.HasValue)
									return x.Value;
								  else
									return 0;
								}).ToArray();	
						
//				string ts1 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",s1.Elapsed.Hours, s1.Elapsed.Minutes, s1.Elapsed.Seconds,s1.Elapsed.Milliseconds / 10);
//				string res1 = "sclr "+epsilon.ToString()+" "+n.ToString()+" "+i.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+Rsq(data,model).ToString()+" "+rs.ToString()+" "+ts1; 
//				s1.Reset();
				s2.Start();
				double[] plr = sec.SGD(epsilon, x=>x);
				s2.Stop();
				string ts2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",s2.Elapsed.Hours, s2.Elapsed.Minutes, s2.Elapsed.Seconds,s2.Elapsed.Milliseconds / 10);
				string res2 = "plr "+epsilon.ToString()+" "+n.ToString()+" "+i.ToString()+" "+sigma.ToString()+" "+mu.ToString()+" "+Rsq(data,plr).ToString()+" "+rs.ToString()+" "+ts2;
				s2.Reset();
				r1.Add(res1);
				r2.Add(res2);
			}

			return r2;
		}
		static List<double[]> clean_data(int d, int n)
		{
			Random rand = new Random();
			List<double[]> data = new List<double[]>();
			
			for (int i = 0; i < n; i++)
			{
				double[] entry = new double[d];
				
				for (int j = 0; j < d-1; j++)
				{
					double u1 = rand.NextDouble();
					double u2 = rand.NextDouble();
					double sample = Math.Sqrt(-2.0*Math.Log(u1))*Math.Sin(2.0*Math.PI*u2);
					entry[j] = sample;
				}

				double x_dist = Math.Sqrt(entry.Select(x=>x*x).Sum());
				entry = entry.Select(x=>x/x_dist).ToArray();
				double uf = rand.NextDouble();
				double ug = rand.NextDouble();
				double fn = Math.Sqrt(-2.0*Math.Log(uf))*Math.Sin(2.0*Math.PI*ug);
				double y = entry.Sum()+fn*0.1;
				entry[d-1] = y > 1 ? 1: y < -1 ? -1: y;
				data.Add(entry);
			}
			return data;
		}
		static void lr_comp(int d, int n, double epsilon) //see if this acutally works
		{
			var data = clean_data(d,n);
			var m = lr(data);

			PINQueryable<double[]> sec = new PINQueryable<double[]>(data.AsQueryable(),null);
			var s = sec.PLR(epsilon, x=>x);

			Console.WriteLine("omega_m = "+string.Join(",",m));
			Console.WriteLine("omega_s = "+string.Join(",",s));
		}	
					
		static void Main(string[] args)
		{
			//invoke mono lr_test.exe <eps> <d> <PTR lr results file> <OP lr results file>
			
			List<string> PTRres = new List<string>();
			List<string> OPres = new List<string>();
//			IEnumerable<int> nlist = new List<int>(new int[7] {100,500,1000,5000,10000,50000,100000});
			IEnumerable<int> nlist = new List<int>(new int[1] {10000});
			foreach (int n in nlist)
			{
				Console.WriteLine("Calculating for "+n.ToString());
				var res = LR_test(0,1,Convert.ToInt32(args[1]),n,Convert.ToDouble(args[0]),100);
				PTRres.AddRange(res);
//				OPres.AddRange(res.Item2);
			}
			System.IO.File.WriteAllLines(args[2],PTRres);
//			System.IO.File.WriteAllLines(args[3],OPres);

			//lr_comp(Convert.ToInt32(args[1]),Convert.ToInt32(args[2]),Convert.ToDouble(args[0]));			
		}
	}
}	
